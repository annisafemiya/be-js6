'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_pc extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_pc.hasOne(models.user_pc_biodatas, { foreignKey: 'user_pc_id', as: 'User_pc_biodatas' })
      user_pc.hasMany(models.user_pc_histories, { foreignKey: 'user_pc_id', as: 'user_pc' })

    }
  }
  user_pc.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    video: DataTypes.STRING,
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_pc',
  });
  return user_pc;
};
