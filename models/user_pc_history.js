'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_pc_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user_pc_history.init({
    user_pc_id: DataTypes.INTEGER,
    list_time: DataTypes.TIME,
    pc: DataTypes.BIGINT
  }, {
    sequelize,
    modelName: 'user_pc_history',
  });
  return user_pc_history;
};
