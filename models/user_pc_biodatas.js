'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_pc_biodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user_pc_biodatas.init({
    name: DataTypes.STRING,
    user_pc_id: DataTypes.INTEGER,
    gender: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    age: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_pc_biodatas',
    tableName: 'user_pc_biodatas',
  });
  return user_pc_biodatas;
};
