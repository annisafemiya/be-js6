const { user_pc_history} = require('../models')

class HistoryController {
    static listHistory(req, res) {
        user_pc_history.findAll().then((data) => { res.status(200).json(data) })
            .catch((error) => { res.status(500).json(error) })
    }

}
module.exports = HistoryController
