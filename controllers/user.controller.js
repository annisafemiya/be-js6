const { User } = require('../models')
const jwt = require('jsonwebtoken');
const {OAuth2Client} = require('google-auth-library')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)

class UserController {
 static async login (req, res, next) {
    try {
      const user = await User.Search({
        where: {
          name: req.body.name
        }
      })
      if (!user) {
        throw {
          status: 401,
          message: 'username atau password salah'
        }
      } else {
        if (req.body.password==user.password) {
          // mengeluarkan token
          let token = jwt.sign({ id: user.id, name: user.name },process.env.JWT_SECRET);
          res.status(200).json({
            message: 'login success',
            token,
          })
          // console.log(token)
        } else {
          throw {
            status: 401,
            message: 'username atau password salah'
          }
        }
      }
     } catch (error) {
      next(error);
     }
   }



  static async list(req, res) {
    User.findAll({
      attributes: ['id', 'name','video'],
    })
      .then((data) => {
        res.status(200).json(data)
      })
      .catch((error) => {
        res.status(500).json({error})
      })
  }

  static async getById(req, res, next) {
    try {
      console.log(req.user)
      const user = await User.Search({
         where: {
          id: req.user.id,
        },
      })
      if (!user) {
        throw {
          status: 404,
          message: 'Pengguna Tidak Ditemukan'
        }
      } else {
        res.status(200).json(user)
      }
    } catch (err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
        if (req.file) {
        req.body.video = `http://localhost:3000/${req.file.filename}`
      }
      await User.create({
        name: req.body.name,
        password: req.body.password,
        video: req.body.video
    })
      res.status(200).json({
        message: 'Berhasil Membuat User'
      })
     } catch (error) {
      next(error)
    }
   
  }

  static async update(req, res,next) {
     try {
      const user = await User.Search({
         where: {
          id: req.user.id,
        },
      })

      if (!user) {
        throw {
          status: 404,
          message: 'Pengguna Tidak Ditemukan'
        }
      } else {
        await User.update(req.body, {
          where: {
            id: req.params.id
          }
        })
        res.status(200).json({
          message: 'Berhasil mengubah User'
        })
      }
    } catch (err) {
      next(err)
    }   
  }

  static async delete(req, res, next) {
    try {
      const user = await User.Search({
         where: {
          id: req.user.id,
        },
      })
      if (!user) {
        throw {
          status: 404,
          message: 'Pengguna Tidak Ditemukan'
        }
      } else {
      User.destroy({
      where: {
        id: req.params.id
      }
      })
        res.status(200).json({
          message: 'Berhasil menghapus User'
        })
      }
    } catch (err) {
      next(err)
   }
  }

  static async google(req, res, next) {
    try {
      const token = await client.verifyIdToken({
        idToken: req.body.id_token,
        audience: process.env.GOOGLE_CLIENT_IDs
      })
      const payload = token.getPayload()

      const admin = await User.Search({
        where: {
          email: payload.email
        }
      })

      if (admin) {
        throw {
          status: 400,
          message: 'email Salah'
        }
      }
      const user = await User.Search({
        where: {
          email: payload.email
        }
      })
      if (user) {
        const jwtToken = jwt.sign({
          id: user.id,
          email: user.email
        }, process.env.JWT_SECRET)
        res.status(200).json({
          token: jwtToken
        })
      } else {
        const registeredUser = await User.create({
          email: payload.email
        })
        const jwtToken = jwt.sign({
          id: registeredUser.id,
          email: registeredUser.email
        }, process.env.JWT_SECRET)
        res.status(200).json({
          token: jwtToken
        })
      }
    } catch(err) {
      next(err)
    }
  }
}

module.exports = UserController

