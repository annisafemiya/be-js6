const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const UserController = require('../controller/user.controller.js')
const { body, validationResult } = require('express-validator');

const multer = require('multer')
const storage = require('../services/multerStorage.service')
const upload = multer({
  storage,
  limits: {
    fileSize: 10000000
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype.match('video')) {
      cb(null, true)
    } else {
      cb(Error, false)
    }
  }
})

router.post('/login-google', UserController.google)

router.post('/', UserController.login)
router.get('/user',
(req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Permintaan Tidak Dapat Diproses'
    }
  }
  }, UserController.list)
  
  
router.get('/user/id',
  (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Permintaan Tidak Dapat Diproses'
      
    }
  }
  },
  body('id').notEmpty(),
  UserController.getById)


router.post('/user',
  upload.single('video'),
  [
  body('name').notEmpty(),
  body('password').notEmpty(),
],
  (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: "Data Tidak Boleh Kosong" 
      }
    } else {
      next()
    }
  },UserController.create
)


router.put('/user/:id',
  (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Permintaan Tidak Dapat Diproses'
    }
  }
},
  [
  body('password')
  .optional()
  .notEmpty()
  ],
  (req, res, next) => {
  const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: "nama dan password harus diisi"
      }
    } else {
      next()
    }
  },
  UserController.update)


router.delete('/user/:id', (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Permintaan Tidak Dapat Diproses'
    }
  }
}, UserController.delete)


  
module.exports = router