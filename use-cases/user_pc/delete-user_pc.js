const deleteUser_pc = ({ db }) => {
  return async function select(info) {
    const { id } = info;
    // delete query
    const res = await db.deleteUser_pc({ id });
    let msg = `User_pc was not deleted, please try again.`;
    if (res == 1) {
      msg = `User_pc deleted successfully.`;
      return msg;
    } else {
      throw new Error(msg);
    }
  };
};

module.exports = deleteUser_pc;
