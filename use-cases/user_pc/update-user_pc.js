const updateUser_pc = ({ Db, patchUser_pcs }) => {
  return async function put({ id, ...info }) {
    let data = patchUser_pcs(id, info);

    data = {
      id: data.getId(),
      firstName: data.getFn(),
      lastName: data.getLn(),
      age: data.getAge(),
    };

    // check id if User_pc exist
    const checkId = await Db.selectOne({ id: data.id });
    if (checkId.rowCount == 0)
      throw new Error(`User_pc doesn't exist, please check.`);

    // check if name exist
    const check = await Db.checkNameExistUpdate({ data });
    if (check.rowCount > 0)
      throw new Error(`User_pc already exist, please check.`);

    // update
    const res = await Db.patchUser_pc({ data });

    let msg = `User_pc was not updated, please try again`;
    if (res[0] == 1) {
      msg = `User_pc updated successfully.`;
      return msg;
    } else {
      throw new Error(msg);
    }
  };
};

module.exports = updateUser_pc;
