const addUser_pc = ({ makeUser_pcs, Db }) => {
  return async function post(info) {
    let data = await makeUser_pcs(info); // entity

    data = {
      firstName: data.getFn(),
      lastName: data.getLn(),
      age: data.getAge(),
    };

    // to do checking if name already exist
    const check = await Db.checkNameExist({ data });
    if (check.rowCount > 0)
      throw new Error(`User_pc already exist, please check.`);
    //   insert
    const res = await Db.insertNewUser_pc({ data });

    // ##
    let msg = `Error on inserting User_pc, please try again.`;

    if (res) {
      msg = `User_pc has been added successfully.`;
      return msg;
    } else {
      throw new Error(msg);
    }
  };
};

module.exports = addUser_pc;
