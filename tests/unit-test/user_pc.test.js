const randomstring = require("randomstring"); // for random string insert and update

// change to test DB
beforeAll(() => {
  process.env.NODE_ENV = "test";
});
// require functions on User_pc
const {
  addUser_pc,
  selectUser_pc,
  updateUser_pc,
  deleteUser_pc,
} = require("../../use-cases/User_pc/app");

describe(`User_pc Tests Suites`, () => {
  test(`Select User_pc`, async () => {
    const info = {};
    const res = await selectUser_pc(info);
    expect(res).toBeDefined();
  });

  test(`Add User_pc - All fields have value.`, async () => {
    const info = {
      firstName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      lastName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      age: 8,
    };
    const res = await addUser_pc(info);
    expect(res).toBe(`User_pc has been added successfully.`);
  });

  test(`Add User_pc - Required fields missing.`, async () => {
    try {
      const info = {
        firstName: randomstring.generate({
          length: 12,
          charset: "alphabetic",
        }), // generate random string
        lastName: null,
        age: 8,
      };
      await addUser_pc(info);
    } catch (e) {
      expect(e.toString()).toBe("Error: Please enter last name.");
    }
  });

  test(`Update User_pc - All fields have value.`, async () => {
    // select last added
    const info = {};
    const emp = await selectUser_pc(info);
    const User_pc = emp[emp.length - 1];
    const User_pcId = User_pc.id;

    const data = {
      id: User_pcId,
      firstName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      lastName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      age: 9,
    };

    const res = await updateUser_pc(data);
    expect(res).toBe(`User_pc updated successfully.`);
  });

  test(`Update User_pc - Required fields are missing.`, async () => {
    try {
      // select last added
      const info = {};
      const emp = await selectUser_pc(info);
      const User_pc = emp[emp.length - 1];
      const User_pcId = User_pc.id;

      const data = {
        id: User_pcId,
        firstName: null,
        lastName: randomstring.generate({
          length: 12,
          charset: "alphabetic",
        }), // generate random string
        age: 9,
      };

      await updateUser_pc(data);
    } catch (e) {
      expect(e.toString()).toBe("Error: Please enter first name.");
    }
  });

  test(`Delete User_pc doesn't exist.`, async () => {
    try {
      // select last added
      const info = {};
      const emp = await selectUser_pc(info);
      const User_pc = emp[emp.length - 1];
      const User_pcId = User_pc.id;
      const data = {
        id: parseInt(User_pcId) + 10, // id that never exist
      };
      await deleteUser_pc(data);
    } catch (e) {
      expect(e.toString()).toBe(
        "Error: User_pc was not deleted, please try again."
      );
    }
  });

  test(`Delete User_pc.`, async () => {
    // select last added
    const info = {};
    const emp = await selectUser_pc(info);
    const User_pc = emp[emp.length - 1];
    const User_pcId = User_pc.id;

    const data = {
      id: User_pcId,
    };

    const res = await deleteUser_pc(data);
    expect(res).toBe(`User_pc deleted successfully.`);
  });
});
