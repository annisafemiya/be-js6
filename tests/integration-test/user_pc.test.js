const randomstring = require("randomstring"); // for random string insert and update

// change to test DB
beforeAll(() => {
  process.env.NODE_ENV = "test";
});

const routes = require("./User_pc"); // routes to be tested

describe(`User_pc Tests Suites`, () => {
  test(`Select User_pc`, async () => {
    const res = await routes.selectUser_pc();
    const data = res.status;
    expect(data).toBe(200);
  });

  test(`Add User_pc - All fields have value.`, async () => {
    const info = {
      firstName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      lastName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      age: 13,
    };
    const res = await routes.User_pc({ info });
    const data = res.status;
    expect(data).toBe(201);
  });

  test(`Add User_pc - Required fields missing.`, async () => {
    const info = {
      firstName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      lastName: null,
      age: 8,
    };
    const res = await routes.User_pc({ info });
    const data = res.response.status;
    expect(data).toBe(400);
  });

  test(`Update User_pc - All fields have value.`, async () => {
    const emp = await routes.selectUser_pc();
    const User_pc = emp.data.view;
    const id = User_pc[User_pc.length - 1].id;
    const info = {
      firstName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      lastName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      age: 9,
    };

    const res = await routes.updateUser_pc({ id, info });
    const data = res.status;
    expect(data).toBe(200);
  });

  test(`Update User_pc - Required fields are missing.`, async () => {
    const emp = await routes.selectUser_pc();
    const User_pc = emp.data.view;
    const id = User_pc[User_pc.length - 1].id;
    const info = {
      firstName: null,
      lastName: randomstring.generate({
        length: 12,
        charset: "alphabetic",
      }), // generate random string
      age: 9,
    };

    const res = await routes.updateUser_pc({ id, info });
    const data = res.response.status;
    expect(data).toBe(400);
  });

  test(`Delete User_pc`, async () => {
    const emp = await routes.selectUser_pc();
    const User_pc = emp.data.view;
    const id = User_pc[User_pc.length - 1].id;

    const res = await routes.deleteUser_pc({ id });
    const data = res.status;
    expect(data).toBe(200);
  });
});
