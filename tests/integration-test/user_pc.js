const axios = require("axios");
require("dotenv").config();
const url = `${process.env.BASE_URL}:${process.env.TEST_PORT}`;

const routes = {
  selectUser_pc: async () => {
    try {
      const res = await axios({
        method: "GET",
        url: `${url}/api/User_pc`,
        auth: {
          username: process.env.name,
          password: process.env.pass,
        },
      });
      return res;
    } catch (e) {
      return e;
    }
  },
  addUser_pc: async ({ info }) => {
    try {
      const res = await axios({
        method: "POST",
        url: `${url}/api/User_pc`,
        auth: {
          username: process.env.name,
          password: process.env.pass,
        },
        data: {
          ...info,
        },
      });
      return res;
    } catch (e) {
      return e;
    }
  },
  updateUser_pc: async ({ id, info }) => {
    try {
      const res = await axios({
        method: "PATCH",
        url: `${url}/api/User_pc/${id}`,
        auth: {
          username: process.env.name,
          password: process.env.pass,
        },
        data: {
          ...info,
        },
      });
      return res;
    } catch (e) {
      return e;
    }
  },
  deleteUser_pc: async ({ id }) => {
    try {
      const res = await axios({
        method: "DELETE",
        url: `${url}/api/User_pc/${id}`,
        auth: {
          username: process.env.name,
          password: process.env.pass,
        },
      });
      return res;
    } catch (e) {
      return e;
    }
  },
};

module.exports = routes;
