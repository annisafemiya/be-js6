'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('user_pc', 'email', { type: Sequelize.STRING });
   
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn('user_pc', 'email', { /* query options */ });
    
  }
};
